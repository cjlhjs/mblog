package ssm.blog.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ssm.blog.entity.Blogger;
import ssm.blog.service.BloggerService;
import ssm.blog.util.MD5Util;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by HJS on 2017/5/14.
 * @Description 博主控制层前台 不需要shiro认证
 */
@Controller
@RequestMapping("/blogger")
public class BloggerController {
    @Resource
    private BloggerService bloggerService;

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public String login(Blogger blogger, HttpServletRequest request){
        //获取登陆实体
        Subject subject= SecurityUtils.getSubject();
        //获取加密后的密码
    String password= MD5Util.md5(blogger.getPassword(),"HJS");
    System.out.println("加密后的密码"+password);
    //获取用户名登陆token
        UsernamePasswordToken token=new UsernamePasswordToken(blogger.getUserName(),password);
        try{
            //根据token登陆，调用MyRealm中的doGetAuthenticationInfo进行身份验证
            subject.login(token);
            return "redirect:/admin/main.jsp";
        }catch (AuthenticationException e){
            e.printStackTrace();
            request.setAttribute("blogger",blogger);
            //提示信息
            request.setAttribute("errorInfo","用户名或者密码错误");
            return "login";
        }
    }
}
