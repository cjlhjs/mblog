package ssm.blog.service;

import ssm.blog.entity.Blogger;


public interface BloggerService {
    /**
     * 查询博主信息
     */
    Blogger getBloggerData();
    /**
     * 通过用户名查询博主信息
     */
    Blogger getBloggerByName(String username);
    /**
     * 更新博主信息
     */
    Integer updateBlogger(Blogger blogger);
}
