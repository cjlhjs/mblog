package ssm.blog.service.impl;

import org.springframework.stereotype.Service;
import ssm.blog.dao.LinkDao;
import ssm.blog.entity.Link;
import ssm.blog.entity.PageBean;
import ssm.blog.service.LinkService;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by HJS on 2017/4/20.
 */
@Service
public class LinkServiceImpl implements LinkService{

    @Resource
    private LinkDao linkDao;


    public List<Link> getTotalData() {
        return linkDao.getTotalData();
    }


    public PageBean<Link> listByPage(PageBean<Link> pageBean) {
        pageBean.setResult(linkDao.listByPage(pageBean.getStart(),pageBean.getEnd()));
        pageBean.setTotal(linkDao.getTotalCount());
        return pageBean;
    }


    public Long getTotalCount() {
        return linkDao.getTotalCount();
    }


    public Integer addLink(Link link) {
        return linkDao.addLink(link);
    }


    public Integer deleteLink(Integer id) {
        return linkDao.deleteLink(id);
    }

    public Integer updateLink(Link link) {
        return linkDao.updateLink(link);
    }
}
