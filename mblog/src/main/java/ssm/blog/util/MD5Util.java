package ssm.blog.util;

import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * Created by HJS on 2017/5/8.
 * @Description MD5加密工具类
 */
public class MD5Util {
    /**
     * @Description 使用Shiro中的md5加密
     * @param str
     * @param salt
     * @return
     */
    public static String md5(String str,String salt){
        return new Md5Hash(str,salt).toString();
    }

    //测试
    public static void main(String[] args) {
        String password="123";
        System.out.println("Md5加密："+MD5Util.md5(password, "HJS"));

    }
}
