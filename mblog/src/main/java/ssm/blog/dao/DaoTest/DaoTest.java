package ssm.blog.dao.DaoTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ssm.blog.dao.BlogDao;
import ssm.blog.dao.BloggerDao;
import ssm.blog.dao.CommentDao;
import ssm.blog.entity.Blog;
import ssm.blog.entity.Comment;
import ssm.blog.entity.Link;
import ssm.blog.entity.PageBean;
import ssm.blog.service.BloggerService;
import ssm.blog.service.LinkService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by HJS on 2017/5/6.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-beans.xml")
public class DaoTest {
    @Resource
    private BlogDao blogDao;
    @Test
    public void listBlog() throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("start",0);
        map.put("end",10);
        List<Blog> blogList=blogDao.listBlog(map);
        for (Blog blog:blogList) {
            System.out.println(blog.toString());
        }
    }
}
