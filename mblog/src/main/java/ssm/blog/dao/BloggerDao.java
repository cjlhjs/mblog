package ssm.blog.dao;

import org.springframework.stereotype.Repository;
import ssm.blog.entity.Blogger;

/**
 * Created by HJS on 2017/5/7.
 * 博主Dao接口
 */
@Repository
public interface BloggerDao {
    /**
     * 查询博主信息
     */
    Blogger getBloggerData();
    /**
     * 通过用户名查询博主信息
     */
    Blogger getBloggerByName(String username);
    /**
     * 更新博主信息
     */
    Integer updateBlogger(Blogger blogger);
}
